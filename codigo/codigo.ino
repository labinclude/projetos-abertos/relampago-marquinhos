/**
 * Relampago Marquinhos
 * 
 * Carrinho radio controlado, construido pelos alunos da turma do Include-BA Lab. Pernambués.
 * katchau!
 * 
 * @authors Jeferson Lima
 * @authors ...
 */

#include <Servo.h>

// ---------------- Funcionalidades ---------------
#include "luzFarol.cpp"
#include "luzFreio.cpp"
#include "luzInterna.h"

// --------------- Car Configuration --------------

#define ENABLE_ESQUERDA_PIN 5
#define ENABLE_DIREITA_PIN 6

#define INA_ESQUERDA_PIN 17
#define INB_ESQUERDA_PIN 16

#define INA_DIREITA_PIN 15
#define INB_DIREITA_PIN 14

#define SERVO_DIRECAO_PIN 9

#define PINO_LUZ_INTERNA 20

/* Servo Motor Object */
Servo direcaoServo;

// ----------------- Arduino Code -----------------

void setup() {
  Serial.begin(115200);
  Serial.println(F("Inicializando Relampago Marquinhos - katchau!..."));

  // Configura funcionalidades
  setupLuzInterna(PINO_LUZ_INTERNA);  
}

void loop() {
  int estadoLuzInterna = false;
  
  // TODO Recebe comandos do controle remoto
  // ...
  
  // Executa as funcionalidades externas
  loopLuzInterna(PINO_LUZ_INTERNA, estadoLuzInterna);

  delay(10);
}
