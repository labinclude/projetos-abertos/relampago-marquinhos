 #ifndef LUZINTERNA_H
 #define LUZINTERNA_H

 #include <Arduino.h>

void setupLuzInterna(int pinoLuzInterna);
void loopLuzInterna(int pinoLuzInterna, int estadoLuzInterna);

 #endif /* LUZINTERNA_H */
