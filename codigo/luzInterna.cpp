/**
 * Luz Interna
 * 
 * Adiciona a funcionalidade 
 * 
 * @authors Icaro Ferreira
 * @authors ...
 */
#include <Arduino.h>

/**
 * Configura a luz interna
 * 
 * @params pinoLuzInterna Define o pino utilizado pela luz interna
 */
void setupLuzInterna(int pinoLuzInterna)
{
  pinMode(pinoLuzInterna, OUTPUT);
}

/**
 * Executa o loop da luz interna
 * 
 * @params pinoLuzInterna Define o pino utilizado pela luz interna
 * @params estadoLuzInterna Define o estado atual da luz interna
 */
void loopLuzInterna(int pinoLuzInterna, int estadoLuzInterna)
{
  if (estadoLuzInterna == HIGH)
  {
    digitalWrite(pinoLuzInterna, HIGH);
  } else {
    digitalWrite(pinoLuzInterna, LOW);
  }
}
